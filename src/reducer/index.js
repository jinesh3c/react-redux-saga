const initialState = {
    article:0
};

const reducer = (state=initialState, action) => {
    const newState = {...state};

    switch(action.type){
        case 'ARTICLE_UP_ASYNC': 
            newState.article += action.value;
            break;
        case 'ARTICLE_DOWN_ASYNC': 
            newState.article -= action.value;
            break;
        case 'GET_NEWS':
            return { ...state, loading: true };
        case 'NEWS_RECEIVED':
            return { ...state, news: action.json[newState.article], loading: false }
        default:
    }
    return newState;
};

export default reducer;