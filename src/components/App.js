import React, { Component } from "react";
import "../App.css";
import { connect } from "react-redux";

import NewsItem from '../containers/NewsItem';
import Loading from '../containers/Loading';
import logo from '../logo.svg';

class App extends Component {
  
  render() {
    return (
      <div className="App">
        <div className="heading">
            <img src={logo} className="logo" alt="logo" />
            <span className="headingTitle">
              <h3>React Redux Saga Example</h3>
            </span>
        </div>
        <div className="news">
             <Loading />
             <NewsItem />
        </div>
        <br/> <br/>
        <button 
          onClick={this.props.onArticleDown}
          >Previos Article</button>
        <button 
        onClick={this.props.onArticleUp}
        >Next Article</button>
      </div>
    );
  }
}
const mapStateToProps = state => {
  return {
    article: state.article
  };
};

const mapDispachToProps = dispatch => {
  return {
    onArticleUp: () => dispatch({ type: "ARTICLE_UP", value: 1 }),
    onArticleDown: () => dispatch({ type: "ARTICLE_DOWN", value: 1 })
  };
};
export default connect(
  mapStateToProps,
  mapDispachToProps
)(App);
