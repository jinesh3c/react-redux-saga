import React from 'react';
import { connect } from 'react-redux'
const imgStyle = {
  hight: 'auto',
  width: '80%',
  border: '1px solid RebeccaPurple ',
};
const articleStyle = {
width: '50%',
margin: '0 auto',
color: 'olive'
}
let NewsItem = ({ article }) => (
article ?
  <article style={articleStyle}>
  <div>
    <h1>{article.title}</h1>
    <img style={imgStyle} src={article.urlToImage} alt="" />
    <h4>{article.description}</h4>
    <a href={article.url} target="_blank">READ MORE</a>
  </div>
</article>
:
<div>
  <h1>No Article</h1>
</div>
);
const mapStateToProps = (state) => ({
article: state.news,
})
NewsItem = connect(mapStateToProps,null)(NewsItem)
export default NewsItem;