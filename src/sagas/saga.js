import { 
	takeEvery, 
	put, 
	takeLatest, 
	all, 
	call, 
	fork 
} from 'redux-saga/effects';

export const delay = (ms) => new Promise(res => setTimeout(res, ms));

function* articleUpAsync() {
	yield call(delay, 1000);
	yield put({
		type: 'ARTICLE_UP_ASYNC', 
		value:1
	});
	yield fetchArticles();
}
function* articleDownAsync() {
	yield call(delay, 1000);
	yield put({
		type: 'ARTICLE_DOWN_ASYNC', 
		value:1
	});
	yield fetchArticles();
}
export function* fetchArticles() {
  const json = yield fetch('https://newsapi.org/v1/articles?source=cnn&apiKey=c39a26d9c12f48dba2a5c00e35684ecc')
    .then(response => response.json(), );    
  yield put({ type: "NEWS_RECEIVED", json: json.articles, });
}

function* watchArticleUp() {
	yield takeLatest('ARTICLE_UP', articleUpAsync);
}
function* watchArticleDown() {
	yield takeEvery('ARTICLE_DOWN', articleDownAsync);
}
function* actionWatcher() {
     yield takeEvery('GET_ARTICLES', fetchArticles)
}
export function* rootSaga(){
	yield all([
	fork(fetchArticles),
    fork(watchArticleUp),
    fork(watchArticleDown),
    fork(actionWatcher),
  ])
}